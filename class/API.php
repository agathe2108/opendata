<?php
class API {
	//Cette classe permet d'utiliser une clé API pour la base de donnée

	public $apikey = 'b9bdcd938f6db99fa99b33c0063d1fbc0c1a0001a3dd799dc2967cd7';
	

	//cette fonction prend en paramètre un url et lui ajoute la clé de l'APLI puis décode le fichier json de l'url
	public function getResultsAPI($url) {
		$url = $url . "&apikey=" . $this->apikey;
		$content = file_get_contents($url);
		if (empty($content)) exit('Error : resultat vide pour url=' . $url);
		$json = json_decode($content, true);
		return $json;
	}


}

