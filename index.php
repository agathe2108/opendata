<?php
//Prend le fichier qui permet d'avoir une api key
require_once('class/API.php');

$vars = $_GET;

?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">

	<head>
		<title>
			Trouver ma formation  
		</title>
		<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js">
		</script>
		<script>
			var etablissement = {};
		</script>
		<?php include('bdd.php');?>



		<link rel="stylesheet" href="style.css"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />

	</head>

	<body >

		<table id= "table">
			<tr>
				<td id="basmenu" colspan="2" >
					<div id="bandeau">
						<a class="active" href="#home"> Toutes les formations </a>
						<a href="index.php">Mon profil</a>
						<a href="index.php">Mes potentielles formations</a>
					</div> 
				</td>
			</tr>
			<tr>
				<td id="menu">
					Menu
					<br>
					1. Region(s)
					<br>

					<form action="index.php" method="get">
						<SELECT multiple class=form-controle name="regions">
							<?php
								$tableau = AfficherFormulaire("https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=reg_etab_lib&refine.rentree_lib=2017-18&apikey=b9bdcd938f6db99fa99b33c0063d1fbc0c1a0001a3dd799dc2967cd7");
								foreach ($tableau as $reg ) {
								    echo "<OPTION>" .$reg;

							}?> 
						</SELECT>
						<br>
						2. Diplomes
						<br>
						<select multiple class= form-control name="diplomes">
							<?php

								$tableau = AfficherFormulaire("https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=diplome_lib&refine.rentree_lib=2017-18&apikey=b9bdcd938f6db99fa99b33c0063d1fbc0c1a0001a3dd799dc2967cd7");
								foreach ($tableau as $dip ) {
								    echo "<OPTION>" .$dip;
								}
							?> 
						</select>
						<br>
						3. Discipline
						<br>
						<select multiple class= form-control name="disciplines">
							<?php
							$tableau = AfficherFormulaire("https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=discipline_lib&refine.rentree_lib=2017-18&apikey=b9bdcd938f6db99fa99b33c0063d1fbc0c1a0001a3dd799dc2967cd7");
							foreach ($tableau as $disc ) {
							    echo "<OPTION>" .$disc;
							}

							?> 
						</select>

						<div class="search">
							<input type="submit" class="searchButton" value="Rechercher">


						</div>
					</form> 
				</td>
				<td id="hautpage">
					<br><br><br>
					Les resultats: 
					<div id="map" ></div>

				</td>
			</tr>

			<tr>
				<td id="resultats" colspan="2" >
					Resultats:
					<br>
						<?php
							foreach ($tableau_affichage as $tab) {
								print $tab;
							}
						?>
				</td>
			</tr>
			<tr>
				<td id="piedpage" colspan="2" >
				</td>
			</tr>

		</table>
		<?php include("map.php");

		?>
	</body>
</html>