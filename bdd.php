<?php 

// ===========================================
//fonctions:

//Permet d'afficher les problèmes dans la console
function console_logOLD($data)
{
    $output = json_encode($data);
    echo "<script>console.log(" . $output . ");</script>";
}


//Affiche les formulaires dans le menu
function AfficherFormulaire($url){
	$contents= file_get_contents($url);
	$valeurs=array();
	$results=json_decode($contents,true);
	$i=0;
	foreach ($results["facet_groups"][0]["facets"] as $value) {
		$valeurs[$i]=$value['name'];
		$i++;
	}
	return $valeurs;
}

//cette variable permet de mettre tous les prints dans un tableau et de l'afficher dans les resultats en dessous la map
$tableau_affichage=array();
$url = 'https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&sort=-rentree_lib&facet=rentree_lib&facet=etablissement_type2&facet=etablissement_type_lib&facet=etablissement&facet=etablissement_lib&facet=champ_statistique&facet=dn_de_lib&facet=cursus_lmd_lib&facet=diplome_rgp&facet=diplome_lib&facet=typ_diplome_lib&facet=diplom&facet=niveau_lib&facet=disciplines_selection&facet=gd_disciscipline_lib&facet=discipline_lib&facet=sect_disciplinaire_lib&facet=spec_dut_lib&facet=localisation_ins&facet=com_etab&facet=com_etab_lib&facet=uucr_etab&facet=uucr_etab_lib&facet=dep_etab&facet=dep_etab_lib&facet=aca_etab&facet=aca_etab_lib&facet=reg_etab&facet=reg_etab_lib&facet=com_ins&facet=com_ins_lib&facet=uucr_ins&facet=dep_ins&facet=dep_ins_lib&facet=aca_ins&facet=aca_ins_lib&facet=reg_ins&facet=reg_ins_lib&refine.rentree_lib=2017-18&apikey=b9bdcd938f6db99fa99b33c0063d1fbc0c1a0001a3dd799dc2967cd7';

/* Code pour créer le lien de ce qu'on a choisi dans le formulaire */
if (!empty($_GET['regions'])) {
	$url .= '&refine.localisation_ins=' . $_GET['regions'];
}

if(!empty($_GET['diplomes'])){
	$url.= '&refine.diplome_rgp='. $_GET['diplomes'];
}

if(!empty($_GET['disciplines'])){
	$url.= '&refine.discipline_lib='. $_GET['disciplines'];
}



$data = file_get_contents($url);
if (empty($data)) exit('ERROR : json empty');
$ecoles = json_decode($data);
$nhits = $ecoles->nhits;
$form_trouve="<br>Nombres de formations trouv&eacute;es: " . $nhits . "<br>";
array_push($tableau_affichage, $form_trouve);
$formations_trouvees=array();
$j=0;
$tableau_cometab=array();

/*Affichage des formations trouvees en dessous de la map*/
foreach ($ecoles->records as $rec) {
	$formation = $rec->fields;

	array_push($tableau_affichage,$formation->etablissement_lib . ", Formation: " . $formation->diplome_rgp . " de " . $formation->discipline_lib . ", Acad&eacute;mie de " . $formation->aca_etab_lib.", etablissement code: ".$formation->com_etab."<br>");
	array_push($tableau_cometab, $formation->com_etab);
	array_push($formations_trouvees, $formation);

	$j++;
}



/* partie joindre les 2 bdd */
$API = new API();

// base de données diplomes
$bdd1 = $API->getResultsAPI("https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=300&sort=-rentree_lib&facet=rentree_lib&facet=etablissement_type2&facet=etablissement_type_lib&facet=etablissement&facet=etablissement_lib&facet=champ_statistique&facet=dn_de_lib&facet=cursus_lmd_lib&facet=diplome_rgp&facet=diplome_lib&facet=typ_diplome_lib&facet=diplom&facet=niveau_lib&facet=disciplines_selection&facet=gd_disciscipline_lib&facet=discipline_lib&facet=sect_disciplinaire_lib&facet=spec_dut_lib&facet=localisation_ins&facet=com_etab&facet=com_etab_lib&facet=uucr_etab&facet=uucr_etab_lib&facet=dep_etab&facet=dep_etab_lib&facet=aca_etab&facet=aca_etab_lib&facet=reg_etab&facet=reg_etab_lib&facet=com_ins&facet=com_ins_lib&facet=uucr_ins&facet=dep_ins&facet=dep_ins_lib&facet=aca_ins&facet=aca_ins_lib&facet=reg_ins&facet=reg_ins_lib&refine.rentree_lib=2017-18");
$rec_bdd1=$bdd1['records'];

// on fait un tableau de markers pour les ajouter à la map après
$markers = array();


foreach ($tableau_cometab as $tableau1) {

	$str= "&refine.com_code=".$tableau1;
	$link="https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&sort=uo_lib&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement".$str;

	//bdd2 est la bdd qui permet d'avoir les coordonnées des ecoles

	$bdd2 = $API->getResultsAPI($link);
	

	foreach($bdd2['records'] as $rec_bdd2){

		if(isset($rec_bdd2['fields']['com_code'])){

			//on recupere la lattitude et la longitude des ecoles que nous avons recherché
			$latitude= $rec_bdd2['fields']['coordonnees'][0];
			$longitude=$rec_bdd2['fields']['coordonnees'][1];

			$urlFormation = $rec_bdd2['fields']['url'];
			$etablissementLib=$rec_bdd2['fields']['uo_lib'];

			//on les ajoute dans le tableau des markers

			$markers[] = "AddMapMarker(".$latitude.",".$longitude.", \"".$etablissementLib."\", \"".$urlFormation."\");";



		}
	}

}


?>