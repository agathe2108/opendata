<script type="text/javascript">

	//fichier de la map leaflet

var map=  L.map('map').setView([48.833, 2.333], 7);

//markerGroup = L.layerGroup().addTo(map);

var osmLayer= L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
attribution: '© OpenStreetMap contributors',
maxZoom: 19

});

map.addLayer(osmLayer);

<?php

//affichage des markers
foreach ($markers as $marker) {
  print $marker;
}
?>

//fonction qui permet d'ajouter des markers à une map
function AddMapMarker(lat, long, name, url){
    var marker =    L.marker([lat, long], {}).addTo(map);
    marker.bindPopup(name + "<br><a target=\"_blank\" href="+url+">Acceder au site web</a>");
}

</script>